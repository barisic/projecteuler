package main

import (
	"bitbucket.org/barisic/projecteuler/assignments"
)

func main() {

	assignments.MultipliesOf3And5()
	assignments.MvenFibonacciNumbersSum(1000)
}
